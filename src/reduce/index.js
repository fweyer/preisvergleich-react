import { combineReducers } from 'redux';
import login from './login_reducer'

const reduce = combineReducers({
    login: login
})

export default reduce