import React, { Component } from "react";
import { Route, HashRouter, Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/esm/FormControl";
import Button from "react-bootstrap/Button";
import "./styles/App.css";
import home from "./component/home";
import ueberUns from "./component/ueberUns";
import kontaktanfrage from "./component/kontaktanfrage";
import preiseingabe from "./component/preiseingabe";
import StickyFooter from 'react-sticky-footer';
import login from "./component/login"
import port from "./pictures/port.jpg"
import logo from "./pictures/logo.png"
import Image from "react-bootstrap/Image";


export default class App extends Component {
    render() {
        return (
            <HashRouter>
                <div>
                    <Navbar bg="dark" expand="lg">
                        <Navbar.Brand href="/">                        
                        <Image style={{ width: 50, height: 50 }} className="rounded mx-auto d-block"
                            src={logo} /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto ">
                                <Link className="mr-3 linkStyle" to="/home">Home</Link>
                                <Link className="mr-3 linkStyle" to="/unternehmen">Für Unternehmen</Link>
                                <Link className="mr-3 linkStyle" to="/ueberUns">Über uns</Link>
                                <Link className="mr-3 linkStyle" to="/kontaktanfrage">Kontakt</Link>
                            </Nav>
                            <Form inline>
                                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                                <Button variant="outline-success">Search</Button>
                            </Form>
                        </Navbar.Collapse>
                    </Navbar>

                    <div className="containerBild">
                        <Image style={{ width:"100%" }} className="rounded mx-auto d-block"
                            src={port} />
                        <div className="textBlock textBlockVisibleFalse">
                            <p>Willkommen bei Flüssiggas-Vergleich.de</p>
                        </div>
                    </div>
                    <br />
                    <div className="content" id="Content">
                        <Route exact path={"/"} component={home} />
                        <Route path={"/home"} component={home} />
                        <Route path={"/ueberUns"} component={ueberUns} />
                        <Route path={"/unternehmen"} component={login} />
                        <Route path={"/kontaktanfrage"} component={kontaktanfrage} />
                        <Route path={"/preiseingabe"} component={preiseingabe} />
                    </div>
                    <StickyFooter>
                        <div className="bg-light pt-1 pb-1 ">
                            <div className="mt-3 container">
                                <div className="row">
                                    <div className="col-sm-4 col-md-4  col-lg-4">
                                        <p>fluessiggas-vergleich.de<br />Abenteuerstraße 16<br />12345 Musterhausen</p>
                                    </div>
                                    <div className="col-sm-4 col-md-4  col-lg-4">
                                        <p>fluessiggas@vergleich.de<br />Tel: 01234/56789<br />Fax: 01234/56789</p>
                                    </div>
                                    <div className="col-sm-4 col-md-4  col-lg-4">
                                        <p>Deutsche Bank<br />IBAN: DE1234 5678 9123 3456 78</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </StickyFooter>
                </div>
            </HashRouter>
        )
    }
}
