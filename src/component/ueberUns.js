import React, {Component} from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import flame from "../pictures/flame.jpg"
import cat from "../pictures/cat.jpg"
import refinery from "../pictures/refinery.jpg"

export default class UeberUns extends Component {

    componentDidMount(prevProps, prevState) {
        window.scrollTo(800,800)
      }

    render() {
        return (
            <div>
                <Row>
                    <Col/>
                    <Col sm={8}>
                        <div className="container">
                        <h2 >Über uns</h2>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut
                            labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                            dolores
                            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                            amet.
                            Dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                            et ea
                            rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut
                            labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                            dolores
                            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                            amet.
                        </p>
                        <p>
                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel
                            illum
                            dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui
                            blandit
                            praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum
                            dolor sit
                            amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                            dolore magna
                            aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper
                            suscipit
                            lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in
                            hendrerit in
                            vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero
                            eros et
                            accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis
                            dolore
                            te feugait nulla facilisi. Nam lib at vero eos et accusdolor sit amet. Lorem ipsum dolor sit
                            amet,
                            consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                            et
                            ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam
                            dolore #
                            dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea
                            et
                            gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum
                            dolor
                            sit amet. Lorem ipsum dolor sit amet, consetetur.
                        </p>
                        <p>
                            At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                            takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
                            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita
                            kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
                            vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim
                            qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        </p>
                        </div>
                    </Col>
                    <Col sm={2} className="mt-4 mb-4">
                        <Image style={{width: 150, height: 150}} className="rounded mx-auto d-block"
                               src={flame}/>
                        <br/>
                        <Image style={{width: 150, height: 150}} className="rounded mx-auto d-block"
                               src={cat}/>
                        <br/>
                        <Image style={{width: 150, height: 150}} className="rounded mx-auto d-block"
                               src={refinery}/>
                    </Col>
                    <Col sm={1}/>
                </Row>
            </div>
        )
    }
}