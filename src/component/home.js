import React, {Component} from "react";
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import gastank from "../pictures/gastank.png";
import "../styles/App.css";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import axios from 'axios';
import Container from "react-bootstrap/Container";
import UnternehmenDTO from "../model/unternehmenDTO"

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            behaelter: "",
            plz: "",
            anbieter: [],
            intervalId: 0
        };

        this.setBehaelter=this.setBehaelter.bind(this)
    }

    componentDidMount(prevProps, prevState) {
        window.scrollTo(800,800)
      }

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value})
    }

    setBehaelter(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }


    submitHandler = e => {
        e.preventDefault()
        console.log(this.state)
        axios.post('http://localhost:8080/preis/anfrage', this.state)
            .then(response => {
                console.log(response)
                this.setState({anbieter: response.data})
                if(response.data === ""){
                    alert("Keine Preise für diese Postleitzahl vorhanden")
                }
            })
            .catch(error => {
                console.log(error.response.data)
                alert(error.response.data)
            })
    }
    render() {
        const {plz} = this.state
        let anbieterDaten = []
        let unternehmen = new UnternehmenDTO()
        for (unternehmen of this.state.anbieter) {
            anbieterDaten.push(
                <div key={unternehmen.id}  className="anbieterDaten">
                    <h3>{unternehmen.name}</h3>
                    <h6>Adresse: {unternehmen.adresse}, {unternehmen.plz} {unternehmen.ort}</h6>
                    <h6>Preis je 100 Liter:</h6>
                    <h5  className="borderText">{unternehmen.preis}</h5>
                </div>
            )
        }

        return (
            <div>
                <div className="container">
                    <article  className="preisvergleich">
                        <h1>Preisvergleich der Anbieter</h1>
                        <div>{anbieterDaten}</div>
                    </article>
                </div>
                <br/><br/>

                <Container>
                <Row>
                    <Col>
                    <h3 className="text-center">Wählen Sie die Optionen für den Preisvergleich aus:</h3>
                    </Col>
                </Row>
                <Row>
                    <Col><h4 className="text-center"><br/>1. Behältergröße</h4></Col>
                </Row>

                <div className="selected-behltergrsse">
                    <form  onSubmit={this.submitHandler}>
                        <div className="text-center" onChange={this.setBehaelter}>
                            <Row>
                                <Col>
                                    <label className="">
                                        <input type="radio" name="behaelter" value="preis2700liter" id="1.2t"/>
                                        <Image src={gastank} className="text-center" height="120" width="120"/>
                                        <br/><h6 className="text-center">1,2 Tonnen</h6>
                                    </label>
                                </Col>
                                <Col>
                                    <label className="">
                                        <input type="radio" name="behaelter" value="preis4850liter" id="2.1t"/>
                                        <Image src={gastank} height="120" width="120"/>
                                        <br/><h6 className="text-center">2,1 Tonnen</h6>
                                    </label>
                                </Col>
                                <Col>
                                    <label className="">
                                        <input type="radio" name="behaelter" value="preis6400liter" id="2.9t"/>
                                        <Image src={gastank} height="120" width="120"/>
                                        <br/><h6 className="text-center">2,9 Tonnen</h6>
                                    </label>
                                </Col>
                            </Row>
                        </div>
                        <Row>
                            <Col><h4 className="text-center"><br/>2. Postleitzahl</h4></Col>
                        </Row>
                        <Row  className="centerManuel text-center">
                            <Col>
                                <br/>
                                <InputGroup className="mb-3 text-center">
                                    <InputGroup.Prepend  >
                                        <InputGroup.Text  id="basic-addon1">PLZ</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <input type="number"  aria-describedby="basic-addon1" name="plz" required="required"
                                           onChange={this.handleChange} value={plz} className="form-control "/>
                                </InputGroup>
                            </Col>
                        </Row>
                        <Row>                          
                            <Col><h4 className="text-center"><br/>3. Losschicken</h4></Col>                     
                        </Row>
                        <Row>                          
                            <Col>
                                <br/>
                                <div className="text-center">
                                    <Button type="submit" className="btn btn-primary"
                                            onClick={this.ladeAnbieter}> Anbieter finden</Button>
                                </div>
                            </Col>
                        </Row>
                        <Row><br/></Row>
                    </form>
                </div>
                </Container>
            </div>
        )
    }
}


