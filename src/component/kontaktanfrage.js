import React, {Component} from "react"
import axios from "axios";
import Card from "react-bootstrap/Card";

export default class Kontaktanfrage extends Component {
    constructor(props){
        super(props)
        this.state = {
            firmennamen: '',
            firmenadresse: '',
            plz: '',
            ort: '',
            kontaktperson:'',
            betreff:'',
            emailAdresse:'',
            nachricht:'',
        };

        this.mySubmitHandler = this.mySubmitHandler.bind(this)
        this.myChangeHandler = this.myChangeHandler.bind(this)
        this.clearStateAndInput = this.clearStateAndInput.bind(this)
    }

    mySubmitHandler = (event) => {
        event.preventDefault();
        console.log(this.state)
        axios.post('http://localhost:8080//kontakt/kontaktUnternehmen', this.state)
            .then(response => {
                if(response.data==="erfolgreich"){
                    console.log(response.data)
                    alert("Ihre Nachricht wurde erfolgreich übermittelt. Wir werden uns zeitnah bei Ihnen melden!")
                }
            })
            .catch(error => {
                console.log(error.response.data)
                alert(error.response.data)
            })
            this.clearStateAndInput()
        }

    clearStateAndInput = () => {
        this.setState({                 
            firmennamen: '',firmenadresse: '', plz: '', ort: '',
            kontaktperson:'', betreff:'', emailAdresse:'', nachricht:'',})

        this.firmennamen.value = ''; this.firmenadresse.value = ''; this.plz.value = ''
        this.ort.value = ''; this.kontaktperson.value = ''; this.betreff.value = ''
        this.emailAdresse.value = ''; this.nachricht.value = ''
    }

    myChangeHandler = (event) => {
        this.setState({[event.target.name] : event.target.value})
    }

    componentDidMount(prevProps, prevState) {
        window.scrollTo(800,800)
      }


    render() {
        return (
            <div>
                <Card className="card-central-kontaktformular elementCenter">
                    <Card.Header><h3>Kontaktformular</h3></Card.Header>
                    <Card.Body>
                        <Card.Title>Sie möchten mit uns zusammenarbeiten?</Card.Title>
                        <Card.Title>Dann schreiben Sie uns eine Nachricht:</Card.Title>
                        <Card.Text>
                                <label htmlFor="text">Firmenname</label>
                                <input type="text" className="form-control" name="firmennamen"
                                        value={this.props.firmennamen}
                                        ref={(el) => (this.firmennamen = el)} 
                                        onChange={this.myChangeHandler} required="required"/>

                                <label htmlFor="text">Adresse</label>
                                <input type="text" className="form-control" name="firmenadresse"
                                        value={this.props.firmenadresse}
                                        ref={(el) => (this.firmenadresse = el)}
                                        onChange={this.myChangeHandler} required="required"/> 

                                <label htmlFor="text">PLZ</label>
                                <input type="number" className="form-control" name="plz" 
                                        value={this.props.plz}
                                        ref={(el) => (this.plz = el)}
                                        onChange={this.myChangeHandler} required="required"/>

                                <label htmlFor="text">Ort</label>
                                <input type="text" className="form-control" name="ort" 
                                        value={this.props.ort}
                                        ref={(el) => (this.ort = el)}
                                        onChange={this.myChangeHandler} required="required"/>

                                <label htmlFor="text">Kontaktperson</label>
                                <input type="text" className="form-control" name="kontaktperson"
                                        value={this.props.kontaktperson}
                                        ref={(el) => (this.kontaktperson = el)}
                                        onChange={this.myChangeHandler} required="required"/>   

                                <label htmlFor="text">Betreff</label>
                                <input type="text" className="form-control" name="betreff"
                                        value={this.props.betreff}
                                        ref={(el) => (this.betreff = el)}
                                        onChange={this.myChangeHandler} required="required"/>

                                <label htmlFor="email">Email</label>
                                <input type="text" className="form-control" name="emailAdresse"
                                        value={this.props.email}
                                        ref={(el) => (this.emailAdresse = el)}
                                        onChange={this.myChangeHandler} required="required"/>

                                <label htmlFor="pwd">Nachricht</label>
                                <textarea className="form-control" name="nachricht" 
                                    value={this.props.nachricht}
                                    ref={(el) => (this.nachricht = el)}
                                    onChange={this.myChangeHandler} rows="6" required="required">
                                </textarea>
                                <br/>
                                <button type="submit" className="btn btn-primary" onClick={this.mySubmitHandler}>
                                    Abschicken
                                </button>
                        </Card.Text>
                    </Card.Body>
                </Card>
                <br/>
            </div>

        );
    }
}
