import React, {Component} from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import store from '../index'
import Card from "react-bootstrap/Card";

export default class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loginName: '',
            loginPasswort: ''
        };

        this.mySubmitHandler = this.mySubmitHandler.bind(this)
        this.myChangeHandler = this.myChangeHandler.bind(this)
    }

    mySubmitHandler = (event) => {
        event.preventDefault();

        axios.post('http://localhost:8080/login/logincheck', this.state)
            .then(response => {
                console.log(response)
                if (response.data === "erfolgreich") {
                    store.dispatch({type:"LOGIN_ERFOLGREICH", state: this.state})
                    this.props.history.push({
                        pathname: '/preiseingabe',
                    })
                }
            })
            .catch(error => {
               console.log(error.response.data)
               alert(error.response.data)       
            })

    }

    myChangeHandler = (event) => {
        this.setState({[event.target.name]: event.target.value})
    }
    componentDidMount(prevProps, prevState) {
        window.scrollTo(800,800)
      }

    render() {
        return (
            <div className="container">
            <Card className="centerManuel">
                <Card.Header><h3>Anmeldung</h3></Card.Header>
                <Card.Body>
                    <Card.Title>Hier können Sie sich einlogen:</Card.Title>
                    <Card.Text>
                    <input type="text" className="form-control" name="loginName"
                                placeholder="Benutzername"
                                value={this.props.loginName} onChange={this.myChangeHandler} required="required"/>
                    <br/>
                    <input type="password" className="form-control" name="loginPasswort"
                            placeholder="Passwort"
                            value={this.props.loginPasswort} onChange={this.myChangeHandler} required="required"/>
                     <br/>
                    <button target="einlogen" type="submit" className="btn btn-primary mt-2 mr-1"
                            placeholder="Name" onClick={this.mySubmitHandler}>Einloggen
                    </button>

                    <label className="d-flex justify-content-center links mt-3">
                        Sie haben noch keine Anmeldedaten?
                        <Link to="/kontaktanfrage">Anmeldung</Link>
                    </label>
                    <br/>
                    </Card.Text>
                </Card.Body>
            </Card>
            <br/>
            </div>
        )
    }
}
