import React, {Component} from "react";
import axios from "axios";
import Card from "react-bootstrap/Card";
import store from '../index'


export default class Preiseingabe extends Component{
  

    constructor(props){
        super(props)
        this.state = {
            preis2700Liter: '',
            preis4850Liter: '',
            preis6400Liter: '',
            postleitzahl: '',
            benutzername: store.getState().login[0].loginName, 
            kennwort: store.getState().login[0].loginPasswort
        };
        this.mySubmitHandler = this.mySubmitHandler.bind(this)
        this.myChangeHandler = this.myChangeHandler.bind(this)
        this.clearStateAndInput = this.clearStateAndInput.bind(this)
    }


    mySubmitHandler = (event) => {
        event.preventDefault();
        console.log(this.state) 
        axios.post('http://localhost:8080//preis/preiseingabe', this.state)
            .then(response => {
                console.log(response)
                if(response.data==="erfolgreich"){
                    alert("Daten wurden erfolgreich in die Datenbank übertragen.")
                } 
            })
            .catch(error => {
                console.log(error.response.data)
                alert(error.response.data)
            })
        
            this.clearStateAndInput()
    }

    myChangeHandler = (event) => {
        this.setState({[event.target.name] : event.target.value})
    }
    
    clearStateAndInput = () => {
        this.setState({                 
            preis2700Liter: '',preis4850Liter: '', preis6400Liter: '',
            postleitzahl:''})

        this.preis2700Liter.value = ''; this.preis4850Liter.value = '' 
        this.preis6400Liter.value = ''; this.postleitzahl.value = ''
    }

    render() {
        return (
            <div>
                <Card className="card-central">
                    <Card.Header className="card-header-central"><h3>Preiseingabe</h3></Card.Header>
                    <Card.Body>
                        <Card.Title>Trage die Preise für die jeweilige PLZ ein:</Card.Title>
                        <Card.Text className="card-text-central">
                     
                            <label htmlFor="text">Preis (je 100 Liter) bei 2700 Liter Tank</label>
                            <input type="number" value={this.props.preis2700Liter} className="form-control"
                                    ref={(el) => (this.preis2700Liter = el)}
                                    name="preis2700Liter" onChange={this.myChangeHandler} required="required"/>
                        
                        
                            <label htmlFor="text">Preis (je 100 Liter) bei 4850 Liter Tank</label>
                            <input type="number" value={this.props.preis4850Liter} className="form-control"
                                    ref={(el) => (this.preis4850Liter = el)}
                                    name="preis4850Liter" onChange={this.myChangeHandler} required="required"/>
                    
                        
                            <label htmlFor="text">Preis (je 100 Liter) bei 6400 Liter Tank</label>
                            <input type="number" value={this.props.preis6400Liter}  className="form-control"
                                    ref={(el) => (this.preis6400Liter = el)}
                                    name="preis6400Liter" onChange={this.myChangeHandler} required="required"/>
                
                        
                            <label htmlFor="text">Postleitzahl</label>
                            <input type="number" value={this.props.postleitzahl}  className="form-control"
                                    ref={(el) => (this.postleitzahl = el)} 
                                    name="postleitzahl" onChange={this.myChangeHandler} required="required"/>
                    
                            <br/>
                            <button type="submit" className="btn btn-primary" onClick={this.mySubmitHandler}>Daten
                                aktualisieren
                            </button>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
