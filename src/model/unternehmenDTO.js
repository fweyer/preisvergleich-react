
class UnternehmenDTO {
   
    constructor(id, name, adresse, plz, ort, preis){
        this.id = id;
        this.name = name;
        this.adresse = adresse;
        this.plz = plz;
        this.ort = ort;
        this.preis=preis;
    }
}

export default UnternehmenDTO;
